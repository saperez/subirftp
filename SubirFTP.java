/**
 *      @author: Saray Pérez
 *      Aplicación que se conecta a un servidor ftp y sube un fichero a éste.
 */

package servidor;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;

import javax.swing.*;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;

public class SubirFTP {

    private static String ip = "192.168.0.160";
    private static Integer puerto = 21;
    private static String usuario = "formacion";
    private static String pass = "formacion";
    private static String fichero_local = "/Users/Sarai/Documents/PSP/tareas/subirtfp/src/main/java/servidor/archivo.txt";
    private static String fichero_servidor = "archivo.txt";
    private static FTPClient ftp;


    public static void main(String[] args) throws IOException {

        conectar();
        cargarArchivoFTP();
        desconectar();

    }

    public static void conectar(){

        try {

            ftp = new FTPClient();
            ftp.connect(ip, puerto);
            ftp.login(usuario, pass);

            int respuesta = ftp.getReplyCode();
            if (FTPReply.isPositiveCompletion(respuesta)) {
                System.out.println("Conectado correctamente al servidor");
                JOptionPane.showMessageDialog(null, "Conectado correctamente al servidor");
            }
            else{
                System.out.println("Imposible conectar al servidor");
                JOptionPane.showMessageDialog(null, "Imposible conectar al servidor");
            }

        }catch (Exception e) {
            System.out.println(e.getMessage());
            System.out.println("Error");
        }
    }

    public static void cargarArchivoFTP() throws IOException {

        ftp.changeWorkingDirectory("/home/formacion");
        ftp.setFileType(FTP.BINARY_FILE_TYPE);

        BufferedInputStream buffIn;
        buffIn=new BufferedInputStream(new FileInputStream(fichero_local));
        ftp.enterLocalPassiveMode();

        if (ftp.storeFile(fichero_servidor, buffIn)) {
            System.out.println("Archivo enviado");
            JOptionPane.showMessageDialog(null, "Archivo enviado");

        }
        //ftp.setUseEPSVwithIPv4(true);
        //ftp.completePendingCommand();

        //Mostramos los archivos que contiene la carpeta
        for (FTPFile file : ftp.listFiles()) {
            System.out.printf("%s %s [%d bytes]\n" ,(file.isDirectory() ? "[D]" : "   "),
                    file.getName(), file.getSize());
        }

        buffIn.close();

    }

    private static void desconectar() {

        try {

            // Cerrar sesión
            ftp.logout();

            // Desconectarse al servidor
            ftp.disconnect();


        } catch (IOException e) {
            e.printStackTrace();

        }
    }

}